//  ############################################################################################
//! @file       RooBrokenPL.cxx
//! @brief      Source for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooBrokenPL.hh"  // rootail: this class
#include "TMath.h"         // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooBrokenPL)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooBrokenPL::RooBrokenPL(const char *name, const char *title,
			 RooAbsReal& x, RooAbsReal& indexLow, RooAbsReal& indexHigh,
			 RooAbsReal& xBreak, RooAbsReal& sharpness) 
  : RooAbsTailPdf(name,title,x),
    _indexLow("_indexLow","IndexLow",this,indexLow),
    _indexHigh("_indexHigh","IndexHigh",this,indexHigh),
    _xBreak("_xBreak","XBreak",this,xBreak),
    _sharpness("_sharpness","Sharpness",this,sharpness)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooBrokenPL::RooBrokenPL(const RooBrokenPL& other, const char* name)
  : RooAbsTailPdf(other,name),
    _indexLow("_indexLow",this,other._indexLow),
    _indexHigh("_indexHigh",this,other._indexHigh),
    _xBreak("_xBreak",this,other._xBreak),
    _sharpness("_sharpness",this,other._sharpness)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Evaluate the PDF
Double_t RooBrokenPL::evaluate() const
{
  Double_t val = xVal();
  if(_sharpness == 0 || _indexLow == _indexHigh)
    return TMath::Power(val, -_indexLow);
  else
    return TMath::Power(val/_xBreak, -_indexLow) * 
           TMath::Power(1 + TMath::Power(val/_xBreak,1./_sharpness), (_indexLow-_indexHigh)*_sharpness);
}

//  end RooBrokenPL.cxx
//  ############################################################################################
