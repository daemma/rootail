//  ############################################################################################
//! @file       RooStretchedExp.cxx
//! @brief      Source for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooStretchedExp.hh"  // rootail: this class
#include "RooRandom.h"         // roofit: random numbers
#include "TMath.h"             // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooStretchedExp)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooStretchedExp::RooStretchedExp(const char *name, const char *title,
				 RooAbsReal& x, RooAbsReal& index, RooAbsReal& lambda) 
  : RooAbsTailPdf(name,title,x),
    _index("_index","Index",this,index),
    _lambda("_lambda","Lambda",this,lambda)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooStretchedExp::RooStretchedExp(const RooStretchedExp& other, const char* name)
  : RooAbsTailPdf(other,name),
    _index("_index",this,other._index),
    _lambda("_lambda",this,other._lambda)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Evaluate the PDF
Double_t RooStretchedExp::evaluate() const
{
  Double_t val = xVal();
  return TMath::Power(val, _index - 1) * TMath::Exp(-_lambda * TMath::Power(val ,_index)) ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Inform the base class that an analytic integral is available
Int_t RooStretchedExp::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, 
				      const char* /*rangeName*/) const 
{
  if (matchArgs(allVars,analVars,_x)) return 1 ;
  return 0 ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the analytic integral
Double_t RooStretchedExp::analyticalIntegral(Int_t code, const char* rangeName) const 
{
  assert(code==1);


  Double_t xmin = calcXVal(_x.min(rangeName));
  Double_t xmax = calcXVal(_x.max(rangeName));
  
  // check for "infinite" range
  if(RooNumber::isInfinite(xmax)) 
    return TMath::Exp(-_lambda * TMath::Power(xmin,_index)) / (_index * _lambda) ;

  return (TMath::Exp(-_lambda * TMath::Power(xmin,_index)) - 
	  TMath::Exp(-_lambda * TMath::Power(xmax,_index))) / (_index * _lambda) ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Inform the base class that a generator is available
Int_t RooStretchedExp::getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const
{
  if (matchArgs(directVars,generateVars,_x)) return 1 ;  
  return 0 ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Generate a random value drawn from this PDF
void RooStretchedExp::generateEvent(Int_t code)
{
  assert(code==1) ;

  Double_t xgen, r ;
  while(1) {    
    r = RooRandom::uniform();
    xgen = TMath::Power( (TMath::Power(_x.min(),_index) - 
			  TMath::Log(1-r)/_lambda), 1/_index );
    if (xgen<_x.max() && xgen>_x.min()) {
      _x = calcXVal(xgen) ;
      break;
    }
  }
  return;
}

//  end RooStretchedExp.cxx
//  ############################################################################################
