//  ############################################################################################
//! @file       RooPowerlaw.cxx
//! @brief      Source for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooPowerlaw.hh"  // rootail: this class
#include "RooRandom.h"     // roofit: random numbers
#include "TMath.h"         // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooPowerlaw)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooPowerlaw::RooPowerlaw(const char *name, const char *title,
			 RooAbsReal& x, RooAbsReal& x0, RooAbsReal& index) 
  : RooAbsTailPdf(name,title,x),
    _x0("_x0","x0",this,x0),
    _index("_index","Index",this,index)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooPowerlaw::RooPowerlaw(const RooPowerlaw& other, const char* name)
  : RooAbsTailPdf(other,name),
    _x0("_x0",this,other._x0),
    _index("_index",this,other._index)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Evaluate the PDF
Double_t RooPowerlaw::evaluate() const
{
  //return TMath::Power(_x, -_index);
  if(!checkXVal()) return 1./RooNumber::infinity();
  return TMath::Power(xVal()/_x0, -_index);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Inform the base class that an analytic integral is available
Int_t RooPowerlaw::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, 
				      const char* /*rangeName*/) const 
{
  if (matchArgs(allVars,analVars,_x)) return 1 ;
  return 0 ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the analytic integral
Double_t RooPowerlaw::analyticalIntegral(Int_t code, const char* rangeName) const 
{
  assert(code==1);

  Double_t xmin = calcXVal(_x.min(rangeName));
  Double_t xmax = calcXVal(_x.max(rangeName));
  Double_t px0 = TMath::Power(_x0,_index);  
  Double_t ind = _index - 1;  

  if(!checkXVal(xmin)){
    if(_discrete) xmin = 1;
    else xmin = 1./RooNumber::infinity();
  } 
  Bool_t isInf = RooNumber::isInfinite(xmax);
  
  if(isInf){
    // check for "infinite" range
    if(_index <= 1)  return RooNumber::infinity();
    return px0 * TMath::Power(xmin,-ind) / ind ;
  } 
  else{
    // finite
    if(_index == 1)  return px0 * TMath::Log(xmax/xmin);
    return px0 * ( TMath::Power(xmin,-ind) - 
		   TMath::Power(xmax,-ind) ) / ind ;
  }
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Inform the base class that a generator is available
Int_t RooPowerlaw::getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const
{
  if (matchArgs(directVars,generateVars,_x)) return 1 ;  
  return 0 ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Generate a random value drawn from this PDF
void RooPowerlaw::generateEvent(Int_t code)
{
  assert(code==1) ;

  Double_t xgen, r ;
  while(1) {    
    r = RooRandom::uniform();
    xgen = _x.min() * TMath::Power(r, -1/(_index-1));
    if (xgen<_x.max() && xgen>_x.min()) {
      _x = calcXVal(xgen) ;
      break;
    }
  }
  return;
}

//  end RooPowerlaw.cxx
//  ############################################################################################
