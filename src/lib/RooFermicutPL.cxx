//  ############################################################################################
//! @file       RooFermicutPL.cxx
//! @brief      Source for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooFermicutPL.hh"  // rootail: this class
#include "TMath.h"           // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooFermicutPL)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooFermicutPL::RooFermicutPL(const char *name, const char *title,
			     RooAbsReal& x, RooAbsReal& index, 
			     RooAbsReal& xBreak, RooAbsReal& lambda) 
  : RooAbsTailPdf(name,title,x),
    _index("_index","Index",this,index),
    _xBreak("_xBreak","XBreak",this,xBreak),
    _lambda("_lambda","Lambda",this,lambda)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooFermicutPL::RooFermicutPL(const RooFermicutPL& other, const char* name)
  : RooAbsTailPdf(other,name),
    _index("_index",this,other._index),
    _xBreak("_xBreak",this,other._xBreak),
    _lambda("_lambda",this,other._lambda)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Evaluate the PDF
Double_t RooFermicutPL::evaluate() const
{
  Double_t xxx = xVal();
  return TMath::Power(xxx, -_index) / 
         (1 + TMath::Power(xxx/_xBreak, 1./(TMath::Ln10() * _lambda)));
}

//  end RooFermicutPL.cxx
//  ############################################################################################
