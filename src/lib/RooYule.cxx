//  ############################################################################################
//! @file       RooYule.cxx
//! @brief      Source for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooYule.hh"  // rootail: this class
#include "RooRandom.h"     // roofit: random numbers
#include "TMath.h"         // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooYule)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooYule::RooYule(const char *name, const char *title,
		 RooAbsReal& x, RooAbsReal& index) 
  : RooAbsTailPdf(name,title,x),
    _index("_index","Index",this,index)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooYule::RooYule(const RooYule& other, const char* name)
  : RooAbsTailPdf(other,name),
    _index("_index",this,other._index)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Evaluate the PDF
Double_t RooYule::evaluate() const
{
  if(!checkXVal()) return 1./RooNumber::infinity();
  Double_t val = xVal();
  return TMath::Gamma(val) / TMath::Gamma(val + _index);
}

//  end RooYule.cxx
//  ############################################################################################
