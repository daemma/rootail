//  ############################################################################################
//! @file       RooGenExponential.cxx
//! @brief      Source for generalized exponential PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooGenExponential.hh"  // rootail: this class
#include "RooRandom.h"           // roofit: random numbers
#include "TMath.h"               // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooGenExponential)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooGenExponential::RooGenExponential(const char *name, const char *title,
				     RooAbsReal& x, RooAbsReal& xS, RooAbsReal& xC) 
  : RooAbsTailPdf(name,title,x),
    _xS("_xS","xScale",this,xS),
    _xC("_xC","xCut",this,xC),
    _Eta(0)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooGenExponential::RooGenExponential(const RooGenExponential& other, const char* name)
  : RooAbsTailPdf(other,name),
    _xS("_xS",this,other._xS),
    _xC("_xC",this,other._xC),
    _Eta(other._Eta)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Evaluate the PDF
Double_t RooGenExponential::evaluate() const
{
  if(!checkXVal()) return 1./RooNumber::infinity();
  return 1. / (TMath::Exp(TMath::Abs(xVal()-_xC)/_xS) + _Eta);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Inform the base class that an analytic integral is available
Int_t RooGenExponential::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, 
				      const char* /*rangeName*/) const 
{
  if(matchArgs(allVars,analVars,_x)
     && _Eta==0 && _xC==0.0) return 1 ;
  return 0 ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the analytic integral
Double_t RooGenExponential::analyticalIntegral(Int_t code, const char* rangeName) const 
{
  assert(code==1);
  Double_t xmin = calcXVal(_x.min(rangeName));
  Double_t xmax = calcXVal(_x.max(rangeName));  
  // check for "infinite" range
  if(RooNumber::isInfinite(xmax)) return _xS * TMath::Exp(-xmin/_xS) ;
  //! ?? \todo check this
  if(_xS == 0) return xmax - xmin ;
  return ( TMath::Exp(-xmax/_xS) - TMath::Exp(-xmin/_xS) ) * _xS ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Inform the base class that a generator is available
Int_t RooGenExponential::getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const
{
  if (matchArgs(directVars,generateVars,_x)
      && _Eta==0
      && _xC==0.0) return 1 ;  
  return 0 ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Generate a random value drawn from this PDF
void RooGenExponential::generateEvent(Int_t code)
{
  assert(code==1) ;

  Double_t xgen, r ;
  while(1) {    
    r = RooRandom::uniform();
    xgen = _x.min() - TMath::Log(1-r) / 1./_xS;
    if (xgen<_x.max() && xgen>_x.min()) {
      _x = calcXVal(xgen) ;
      break;
    }
  }
  return;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the decay type
void RooGenExponential::setEta(Int_t eta)
{
  _Eta = eta;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Set the decay type
void RooGenExponential::setEtaType(DecayType dt)
{
  _Eta = static_cast<Int_t>(dt);
}

//  end RooGenExponential.cxx
//  ############################################################################################
