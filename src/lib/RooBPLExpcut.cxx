//  ############################################################################################
//! @file       RooBPLExpcut.cxx
//! @brief      Source for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooBPLExpcut.hh"  // rootail: this class
#include "TMath.h"          // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooBPLExpcut)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooBPLExpcut::RooBPLExpcut(const char *name, const char *title,
			   RooAbsReal& x, RooAbsReal& indexLow, RooAbsReal& indexHigh,
			   RooAbsReal& xBreak, RooAbsReal& xCut, RooAbsReal& xScale) 
  : RooAbsTailPdf(name,title,x),
    _indexLow("_indexLow","IndexLow",this,indexLow),
    _indexHigh("_indexHigh","IndexHigh",this,indexHigh),
    _xBreak("_xBreak","XBreak",this,xBreak),
    _xCut("_xCut","XCut",this,xCut),
    _xScale("_xScale","XScale",this,xScale)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooBPLExpcut::RooBPLExpcut(const RooBPLExpcut& other, const char* name)
  : RooAbsTailPdf(other,name),
    _indexLow("_indexLow",this,other._indexLow),
    _indexHigh("_indexHigh",this,other._indexHigh),
    _xBreak("_xBreak",this,other._xBreak),
    _xCut("_xCut",this,other._xCut),
    _xScale("_xScale",this,other._xScale)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Evaluate the PDF
Double_t RooBPLExpcut::evaluate() const
{
  Double_t val = xVal();
  if(val<=_xBreak && val<=_xCut)
    return TMath::Power(val/_xBreak, _indexLow);
  else if(val<=_xBreak && val>_xCut)
    return TMath::Power(val/_xBreak, _indexLow) * TMath::Exp(-(val-_xCut)/_xScale);
  else if(val>_xBreak && val<=_xCut)
    return TMath::Power(val/_xBreak, _indexHigh);
  else if(val>_xBreak && val>_xCut)
    return TMath::Power(val/_xBreak, _indexHigh) * TMath::Exp(-(val-_xCut)/_xScale);
  else
    return 0.;
}

//  end RooBPLExpcut.cxx
//  ############################################################################################
