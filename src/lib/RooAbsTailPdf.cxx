//  ############################################################################################
//! @file       RooAbsTailPdf.cxx
//! @brief      Source for abstract base class for tail PDFs
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-27
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooAbsTailPdf.hh"  // rootail: this class
#include "RooAbsReal.h"      // roofit: abstract real valued variable
#include "TMath.h"           // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooAbsTailPdf)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooAbsTailPdf::RooAbsTailPdf(const char *name, const char *title, RooAbsReal& x) 
  : RooAbsPdf(name,title),
    _x("_x","Observable",this,x),
    _discrete(kFALSE),
    _protectNegative(kTRUE)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooAbsTailPdf::RooAbsTailPdf(const RooAbsTailPdf& other, const char* name)
  : RooAbsPdf(other,name),
    _x("_x",this,other._x),
    _discrete(other._discrete),
    _protectNegative(other._protectNegative)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Is negative x value disallowed and is x possitive?
Bool_t RooAbsTailPdf::checkXVal(Double_t xraw) const
{
  if(_protectNegative && xraw <= 0) return kFALSE;
  return kTRUE;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Check the value of the proxy variable
Bool_t RooAbsTailPdf::checkXVal() const
{
  return checkXVal(_x);
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the the x value : |x| and floor(x)
Double_t RooAbsTailPdf::calcXVal(Double_t xraw) const
{
  xraw = TMath::Abs(xraw);
  return _discrete ? TMath::Floor(xraw) : xraw;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the the proxy value : |x| and floor(x)
Double_t RooAbsTailPdf::xVal() const
{
  return calcXVal(_x);
}

//  end RooAbsTailPdf.cxx
//  ############################################################################################
