//  ############################################################################################
//! @file       RooExpcutPL.cxx
//! @brief      Source for power-law with exponential cuttoff PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooExpcutPL.hh"  // rootail: this class
#include "RooRandom.h"     // roofit: random numbers
#include "TMath.h"         // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooExpcutPL)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooExpcutPL::RooExpcutPL(const char *name, const char *title,
			 RooAbsReal& x, RooAbsReal& index, RooAbsReal& lambda) 
  : RooAbsTailPdf(name,title,x),
    _index("_index","Index",this,index),
    _lambda("_lambda","Lambda",this,lambda)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooExpcutPL::RooExpcutPL(const RooExpcutPL& other, const char* name)
  : RooAbsTailPdf(other,name),
    _index("_index",this,other._index),
    _lambda("_lambda",this,other._lambda)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Evaluate the PDF
Double_t RooExpcutPL::evaluate() const
{
  Double_t val = xVal();
  return TMath::Power(val, -_index) * TMath::Exp(-_lambda * val);
}

// //_____________________________________________________________________________
// Int_t ExpcutPL::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, 
// 					 const char* /*rangeName*/) const 
// {
//   if (matchArgs(allVars,analVars,_x)) return 1 ;
//   return 0 ;
// }

// //_____________________________________________________________________________
// Double_t ExpcutPL::analyticalIntegral(Int_t code, const char* rangeName) const 
// {
//   assert(code==1);

//   Double_t xmin = _x.min(rangeName);
//   Double_t xmax = _x.max(rangeName);

//   // check for sigularity
//   if(xmin<=0 || xmax<=0){
//     coutE(InputArguments) << "ExpcutPL::analyticalIntegral: " 
// 			  << "ERROR Range contains sigularity (x == 0)" << endl;
//     return 0; 
//   }

//   Double_t ind = _index - 1;  
//   Double_t lam = TMath::Power(_lambda, ind);    
//   // check for "infinite" range
//   if(RooNumber::isInfinite(xmax)) return lam * gsl_sf_gamma_inc(-ind, _lambda * xmin);
//   return lam * ( gsl_sf_gamma_inc(-ind, _lambda * xmin) - 
// 		    gsl_sf_gamma_inc(-ind, _lambda * xmax) ) ;
// }

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Inform the base class that a generator is available
Int_t RooExpcutPL::getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const
{
  if (matchArgs(directVars,generateVars,_x)) return 1 ;  
  return 0 ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Generate a random value drawn from this PDF
void RooExpcutPL::generateEvent(Int_t code)
{
  assert(code==1) ;
  Double_t xgen, r, p, u ;
  while(1) {    
    r = RooRandom::uniform();

    xgen = _x.min() - TMath::Log(r) / _lambda;
    p = TMath::Power(xgen / _x.min(), -_index);

    u = RooRandom::uniform();
    if( u<p && xgen<_x.max() && xgen>_x.min() ) {
      _x = calcXVal(xgen) ;
      break;
    }
  }
  return;
}

//  end RooExpcutPL.cxx
//  ############################################################################################
