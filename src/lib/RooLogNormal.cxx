//  ############################################################################################
//! @file       RooLogNormal.cxx
//! @brief      Source for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#include "RooLogNormal.hh"  // rootail: this class
#include "RooRandom.h"     // roofit: random numbers
#include "TMath.h"         // root: mathematical methods

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! class implementation macro
ClassImp(RooLogNormal)

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Named constructor.
RooLogNormal::RooLogNormal(const char *name, const char *title,
			   RooAbsReal& x, RooAbsReal& mean, RooAbsReal& sigma)
  : RooAbsTailPdf(name,title,x),
    _mean("_mean","Mean",this,mean),
    _sigma("_sigma","Sigma",this,sigma)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Copy contructor, with rename
RooLogNormal::RooLogNormal(const RooLogNormal& other, const char* name)
  : RooAbsTailPdf(other,name),
    _mean("_mean",this,other._mean),
    _sigma("_sigma",this,other._sigma)
{}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Evaluate the PDF
Double_t RooLogNormal::evaluate() const
{
  Double_t val = xVal();
  Double_t z = (TMath::Log(val) - _mean) / _sigma;
  return (1 / val) * TMath::Exp( -z*z / 2. );
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Inform the base class that an analytic integral is available
Int_t RooLogNormal::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, 
				      const char* /*rangeName*/) const 
{
  if (matchArgs(allVars,analVars,_x)) return 1 ;
  return 0 ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Calculate the analytic integral
Double_t RooLogNormal::analyticalIntegral(Int_t code, const char* rangeName) const 
{
  assert(code==1);
  Double_t termmin = TMath::Erfc( (TMath::Log(_x.min(rangeName))-_mean) / 
				  (_sigma*TMath::Sqrt2()) );
  Double_t termmax = TMath::Erfc( (TMath::Log(_x.max(rangeName))-_mean) / 
				  (_sigma*TMath::Sqrt2()) );
  Double_t dem = TMath::Sqrt(2./(TMath::Pi()*_sigma*_sigma));
  return  (termmin-termmax) / dem; 
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Inform the base class that a generator is available
Int_t RooLogNormal::getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const
{
  if (matchArgs(directVars,generateVars,_x)) return 1 ;  
  return 0 ;
}

//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//! Generate a random value drawn from this PDF
void RooLogNormal::generateEvent(Int_t code)
{
  assert(code==1) ;
  Double_t xgen, r, r2, rho, theta, x1, x2;
  while(1) {    
    r = RooRandom::uniform();
    r2 = RooRandom::uniform();
    rho = TMath::Sqrt( -2.*_sigma*_sigma * TMath::Log(1-r) );
    theta = 2. * TMath::Pi() * r2;
    x1 = TMath::Exp(rho*TMath::Sin(theta));
    x2 = TMath::Exp(rho*TMath::Cos(theta));
    // both x1 and x2 are distributed according to the log-normal
    // but we have to pick one, so ...
    if(r<r2) xgen = x1 ;
    else xgen = x2 ;
    if( xgen<_x.max() && xgen>_x.min() ) {
      _x = calcXVal(xgen) ;
      break;
    }
  }
  return;
}

//  end RooLogNormal.cxx
//  ############################################################################################
