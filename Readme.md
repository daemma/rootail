<!--#########################################################################################-->
<!-- @file       Readme.md -->
<!-- @brief      Project overview and information. -->
<!-- @author     "EmmaH" <dev@daemma.io> -->
<!-- @date       2018-05-27 -->
<!-- @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io> -->
<!--             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt> -->

[![License](https://img.shields.io/:license-GPLv3-blue.svg)](https://gitlab.com/daemma/dotfiles/blob/master/License.txt)
![Version](https://img.shields.io/:version-0.0.0-green.svg)

# RooTail
RooTail is a CERN root-system RooFit based plugin library for heavy tailed distributions.

## Dependencies
### Meta
You'll need the typical C/C++ tool set: `make`, `cmake`, `gcc`, etc. (or somethings isomorphic).

### CERN's root-system (optional)
[Root](https://root.cern.ch/) is CERN's massive and rich data analysis library. 
It's in a league of it's own and can be optionally baked into this package. 
You can find root in many package managers but it is generally better to download and compile the 
latest version for yourself.

## Build
Building this library follows the typical `cmake` pattern:
```
 $ mkdir build
 $ cd build
 $ cmake ..
 $ cmake --build .
```
Alternatively, you can use the included `Makefile` as a short-cut:
```
 $ make
```

### Configuration Options
You can edit the `CMakelists.txt` file to suite your needs: the primary build options 
are listed near the top of the file.
Alternatively, you can tweak the build at configure-time with the typical 
`cmake -D<var>=ON|OFF` syntax.

| Variable               | Description   |
| ---------------------- |:------------- |
| BUILD_SHARED_ONLY      | Build only the shared object library | 
| BUILD_APPS             | Build application executables |
| USE_DOXY               | Build doxygen target |
| CMAKE_RULE_MESSAGES    | Add a progress messages |
| CMAKE_VERBOSE_MAKEFILE | Show each command line as it is used |
| CMAKE_PRINT_CONFIGS    | Print configuration variables |


## Credits

## License
[![License](https://img.shields.io/:license-GPLv3-blue.svg)](https://gitlab.com/daemma/dotfiles/blob/master/License.txt) 

RooTail is a CERN root-system RooFit based plugin library for heavy tailed distributions.

Copyright (C) 2017 "Emma Hague" <dev@daemma.io>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/gpl.txt>.

<!--end Readme.md -->
<!--#########################################################################################-->
