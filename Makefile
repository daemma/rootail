### ############################################################################################
##! @file       Makefile
##! @brief      Recipes for running CMake
##! @author     "EmmaH" <dev@daemma.io>
##! @date       2018-05-27
##! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
##!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>

BUILDDIR = build
.SILENT :
.PHONY  : clean rebuild doc
all     : build
bdir    :
	@test -d $(BUILDDIR) || mkdir $(BUILDDIR)
build   : bdir
	@cd $(BUILDDIR) && \
	cmake ..        && \
	cmake --build . -- -j 4 && \
	cd ..
clean   :
	@rm -rf $(BUILDDIR)/*
rebuild : clean build
doc     :
	@cd $(BUILDDIR) && \
	cmake ..        && \
	make clean-doc  && \
	make doc        && \
	cd ..

### end Makefile
### ############################################################################################
