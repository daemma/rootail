//  ############################################################################################
//! @file       RooBrokenPL.hh
//! @brief      Header for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_BROKENPL_HH
#define ROO_BROKENPL_HH
#include "RooAbsTailPdf.hh"  // rootail: Abstract base PDF

//  ********************************************************************************************
//! A two component power-law pdf with a smooth transition.
/*! @f[ f(E) = C \left(\frac{x}{x_{b}}\right)^{-\gamma_{1}} 
    \left(1 + \left(\frac{x}{x_{b}}\right)^{1/S} \right)^{S(\gamma_{2}-\gamma_{1})} @f]
*/
class RooBrokenPL : public RooAbsTailPdf {
public:
  inline RooBrokenPL(){};  //!< Default constructor
  RooBrokenPL(const char *name, const char *title,
	      RooAbsReal& x, RooAbsReal& indexLow, RooAbsReal& indexHigh,
	      RooAbsReal& xBreak, RooAbsReal& sharpness);
  RooBrokenPL(const RooBrokenPL& other, const char* name=0);
  inline virtual TObject* clone(const char* newname) const
                               { return new RooBrokenPL(*this,newname); } //!< cloner
  inline virtual ~RooBrokenPL() { }   //!< Destructor

protected:
  Double_t evaluate() const ;

protected:
  RooRealProxy _indexLow;
  RooRealProxy _indexHigh;
  RooRealProxy _xBreak;
  RooRealProxy _sharpness;
  
private:
  ClassDef(RooBrokenPL,1); 
};

#endif  // end ROO_BROKENPL_HH

//  end RooBrokenPL.hh
//  ############################################################################################
