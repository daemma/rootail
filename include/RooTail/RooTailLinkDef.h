/*  ########################################################################################  */
/*! @file       RooTailLinkDef.h */
/*! @brief      Link definitions for root loadable objects. */
/*! @author     "EmmaH" <dev@daemma.io> */
/*! @date       2018-05-27 */
/*! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io> */
/*!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt> */
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class RooAbsTailPdf+;
#pragma link C++ class RooPowerlaw+;
#pragma link C++ class RooExpcutPL+;
#pragma link C++ class RooBrokenPL+;
#pragma link C++ class RooBPLExpcut+;
#pragma link C++ class RooFermicutPL+;
#pragma link C++ class RooGenExponential+;
#pragma link C++ class RooStretchedExp+;
#pragma link C++ class RooLogNormal+;
#pragma link C++ class RooYule+;

#endif /* end __CINT__ */

/*  end RooTailLinkDef.h */
/*  ########################################################################################  */
