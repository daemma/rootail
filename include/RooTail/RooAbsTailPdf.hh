//  ############################################################################################
//! @file       RooAbsTailPdf.hh
//! @brief      Header for abstract base class for tail PDFs
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-27
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_ABSTAILPDF_HH
#define ROO_ABSTAILPDF_HH
#include "RooAbsPdf.h"     // roofit: abstract PDF
#include "RooRealProxy.h"  // roofit: real variable proxy
class RooRealVar;          // roofit: real variable

//  ********************************************************************************************
//! Generic abstract base class for tail PDFs.
class RooAbsTailPdf : public RooAbsPdf {
public:
  inline RooAbsTailPdf(){};  //!< Default constructor
  RooAbsTailPdf(const char *name, const char *title, RooAbsReal& x);
  RooAbsTailPdf(const RooAbsTailPdf& other, const char* name=0);
  inline virtual ~RooAbsTailPdf() { }   //!< Destructor

  void   setDiscreteX(bool flag = kTRUE){ _discrete = flag; }
  Bool_t discreteX() const { return _discrete; }
  void   protectNegativeX(bool flag = kTRUE){ _protectNegative = flag; }

protected:
  Bool_t   checkXVal(Double_t xraw) const;
  Bool_t   checkXVal() const;
  Double_t calcXVal(Double_t xraw) const;
  Double_t xVal() const;

protected:
  RooRealProxy _x;  
  Bool_t       _discrete;
  Bool_t       _protectNegative;
  
private:
  ClassDef(RooAbsTailPdf,1); 
};

#endif  // end ROO_ABSTAILPDF_HH

//  end RooAbsTailPdf.hh
//  ############################################################################################
