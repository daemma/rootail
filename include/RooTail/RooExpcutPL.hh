//  ############################################################################################
//! @file       RooExpcutPL.hh
//! @brief      Header for power-law with exponential cutoff PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_EXPCUTPL_HH
#define ROO_EXPCUTPL_HH
#include "RooAbsTailPdf.hh"  // rootail: Abstract base PDF

//  ********************************************************************************************
//! Exponential cutoff power-law PDF.
/*! @f[ f(x) = x^{-\alpha} e^{-\lambda x} @f] */
class RooExpcutPL : public RooAbsTailPdf {
public:
  inline RooExpcutPL(){};  //!< Default constructor
  RooExpcutPL(const char *name, const char *title,
	      RooAbsReal& x, RooAbsReal& index, RooAbsReal& lambda);
  RooExpcutPL(const RooExpcutPL& other, const char* name=0);
  inline virtual TObject* clone(const char* newname) const
                               { return new RooExpcutPL(*this,newname); } //!< cloner
  inline virtual ~RooExpcutPL() { }   //!< Destructor

  // Int_t    getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char*) const;
  // Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;
  Int_t    getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const;
  void     generateEvent(Int_t code);

protected:
  RooRealProxy _index;  
  RooRealProxy _lambda;  
  Double_t evaluate() const ;

protected:
  
private:
  ClassDef(RooExpcutPL,1); 
};

#endif  // end ROO_EXPCUTPL_HH

//  end RooExpcutPL.hh
//  ############################################################################################
