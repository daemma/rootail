//  ############################################################################################
//! @file       RooLogNormal.hh
//! @brief      Header for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_LOGNORMAL_HH
#define ROO_LOGNORMAL_HH
#include "RooAbsTailPdf.hh"  // rootail: Abstract base PDF

//  ********************************************************************************************
//! Log-normal PDF.
/*! @f[ f(x) = \frac{1}{x} \exp\left[ -\frac{(\ln(x)-\mu)^{2}}{2\sigma^{2}} \right] @f] */
class RooLogNormal : public RooAbsTailPdf {
public:
  inline RooLogNormal(){};  //!< Default constructor
  RooLogNormal(const char *name, const char *title,
	       RooAbsReal& x, RooAbsReal& mean, RooAbsReal& sigma);
  RooLogNormal(const RooLogNormal& other, const char* name=0);
  inline virtual TObject* clone(const char* newname) const
                               { return new RooLogNormal(*this,newname); } //!< cloner
  inline virtual ~RooLogNormal() { }   //!< Destructor

  Int_t    getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char*) const;
  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;
  Int_t    getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const;
  void     generateEvent(Int_t code);

protected:
  Double_t evaluate() const ;

protected:
  RooRealProxy _mean;  
  RooRealProxy _sigma;  
  
private:
  ClassDef(RooLogNormal,1); 
};

#endif  // end ROO_LOGNORMAL_HH

//  end RooLogNormal.hh
//  ############################################################################################
