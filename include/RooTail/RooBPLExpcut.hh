//  ############################################################################################
//! @file       RooBPLExpcut.hh
//! @brief      Header for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_BPLEXPCUT_HH
#define ROO_BPLEXPCUT_HH
#include "RooAbsTailPdf.hh"  // rootail: Abstract base PDF

//  ********************************************************************************************
//! Exponentially cut-off broken power pdf.
/*! 
    If \f$ E \leq E_{b} \f$ and \f$ E \leq E_{c} \f$
    \f[ f(E) =  (E/E_{b})^{-\gamma_{1}} \f] 
    else if \f$ E \leq E_{b} \f$ and  \f$ E > E_{c} \f$
    \f[ f(E) =  (E/E_{b})^{-\gamma_{1}} \exp(-(E-E_{c})/E_{s}) \f] 
    else if \f$ E > E_{b} \f$ and \f$ E \leq E_{c} \f$
    \f[ f(E) =  (E/E_{b})^{-\gamma_{2}} \f] 
    else if \f$ E > E_{b} \f$ and  \f$ E > E_{c} \f$
    \f[ f(E) =  (E/E_{b})^{-\gamma_{2}} \exp(-(E-E_{c})/E_{s}) \f] 

    Parameters : 
    - #1 "Index1" \f$ \gamma_{1} \f$
    - #2 "Index2" \f$ \gamma_{2} \f$
    - #3 "Break" \f$E_{b}\f$
    - #4 "Cut" \f$E_{c}\f$
    - #5 "ExpScale" \f$E_{s}\f$
*/
class RooBPLExpcut : public RooAbsTailPdf {
public:
  inline RooBPLExpcut(){};  //!< Default constructor
  RooBPLExpcut(const char *name, const char *title,
	       RooAbsReal& x, RooAbsReal& indexLow, RooAbsReal& indexHigh,
	       RooAbsReal& xBreak, RooAbsReal& xCut, RooAbsReal& xScale);
  RooBPLExpcut(const RooBPLExpcut& other, const char* name=0);
  inline virtual TObject* clone(const char* newname) const
                               { return new RooBPLExpcut(*this,newname); } //!< cloner
  inline virtual ~RooBPLExpcut() { }   //!< Destructor

protected:
  Double_t evaluate() const ;

protected:
  RooRealProxy _indexLow;
  RooRealProxy _indexHigh;
  RooRealProxy _xBreak;
  RooRealProxy _xCut;
  RooRealProxy _xScale;
  
private:
  ClassDef(RooBPLExpcut,1); 
};

#endif  // end ROO_BPLEXPCUT_HH

//  end RooBPLExpcut.hh
//  ############################################################################################
