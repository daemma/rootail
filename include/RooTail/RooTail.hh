//  ############################################################################################
//! @file       RooTail.hh
//! @brief      Header for RooTail library
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_TAIL_HH
#define ROO_TAIL_HH

//  ****************************************************************************
/*! @mainpage

    @author     EmmaH
    @date       2018
    @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
                GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>

    @section Overview
    RooTail is a CERN root-system RooFit based plugin library for heavy tailed distributions.
*/

#endif // end ROO_TAIL_HH

//  end RooTail.hh
//  ############################################################################################
