//  ############################################################################################
//! @file       RooFermicutPL.hh
//! @brief      Header for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_FERMICUTPL_HH
#define ROO_FERMICUTPL_HH
#include "RooAbsTailPdf.hh"  // rootail: Abstract base PDF

//  ********************************************************************************************
//! Power-law PDF with a Fermi function style cutoff.
/*! @f[ f(x) = \frac{x^{-\alpha}} {1 + \left(\frac{x}{x_{b}}\right)^{\frac{1}{\ln 10 \lambda}}} @f] */
class RooFermicutPL : public RooAbsTailPdf {
public:
  inline RooFermicutPL(){};  //!< Default constructor
  RooFermicutPL(const char *name, const char *title,
		RooAbsReal& x, RooAbsReal& index, 
		RooAbsReal& xBreak, RooAbsReal& lambda);
  RooFermicutPL(const RooFermicutPL& other, const char* name=0);
  inline virtual TObject* clone(const char* newname) const
                               { return new RooFermicutPL(*this,newname); } //!< cloner
  inline virtual ~RooFermicutPL() { }   //!< Destructor

protected:
  Double_t evaluate() const ;

protected:
  RooRealProxy _index;  
  RooRealProxy _xBreak;  
  RooRealProxy _lambda;  
  
private:
  ClassDef(RooFermicutPL,1); 
};

#endif  // end ROO_FERMICUTPL_HH

//  end RooFermicutPL.hh
//  ############################################################################################
