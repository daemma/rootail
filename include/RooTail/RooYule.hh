//  ############################################################################################
//! @file       RooYule.hh
//! @brief      Header for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_YULE_HH
#define ROO_YULE_HH
#include "RooAbsTailPdf.hh"  // rootail: Abstract base PDF

//  ********************************************************************************************
//! Yule PDF.
/*! @f[ f(x) = \Gamma(x) / \Gamma(x+\alpha) @f] */
class RooYule : public RooAbsTailPdf {
public:
  inline RooYule(){};  //!< Default constructor
  RooYule(const char *name, const char *title,
	  RooAbsReal& x, RooAbsReal& index);
  RooYule(const RooYule& other, const char* name=0);
  inline virtual TObject* clone(const char* newname) const
                               { return new RooYule(*this,newname); } //!< cloner
  inline virtual ~RooYule() { }   //!< Destructor

protected:
  Double_t evaluate() const ;

protected:
  RooRealProxy _index;  
  
private:
  ClassDef(RooYule,1); 
};

#endif  // end ROO_YULE_HH

//  end RooYule.hh
//  ############################################################################################
