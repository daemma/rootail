//  ############################################################################################
//! @file       RooPowerlaw.hh
//! @brief      Header for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_POWERLAW_HH
#define ROO_POWERLAW_HH
#include "RooAbsTailPdf.hh"  // rootail: Abstract base PDF

//  ********************************************************************************************
//! Power-law PDF.
/*! @f[ f(x) = x^{-\alpha} @f] */
class RooPowerlaw : public RooAbsTailPdf {
public:
  inline RooPowerlaw(){};  //!< Default constructor
  RooPowerlaw(const char *name, const char *title,
	      RooAbsReal& x, RooAbsReal& x0, RooAbsReal& index);
  RooPowerlaw(const RooPowerlaw& other, const char* name=0);
  inline virtual TObject* clone(const char* newname) const
                               { return new RooPowerlaw(*this,newname); } //!< cloner
  inline virtual ~RooPowerlaw() { }   //!< Destructor

  Int_t    getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char*) const;
  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;
  Int_t    getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const;
  void     generateEvent(Int_t code);

protected:
  Double_t evaluate() const ;

protected:
  RooRealProxy _x0;  
  RooRealProxy _index;  
  
private:
  ClassDef(RooPowerlaw,1); 
};

#endif  // end ROO_POWERLAW_HH

//  end RooPowerlaw.hh
//  ############################################################################################
