//  ############################################################################################
//! @file       RooStretchedExp.hh
//! @brief      Header for power-law PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_STRETCHEDEXP_HH
#define ROO_STRETCHEDEXP_HH
#include "RooAbsTailPdf.hh"  // rootail: Abstract base PDF

//  ********************************************************************************************
//! Stretched exponential PDF.
/*! @f[ f(x) = C x^{\beta-1} e^{-\lambda x^{\beta}} @f] */
class RooStretchedExp : public RooAbsTailPdf {
public:
  inline RooStretchedExp(){};  //!< Default constructor
  RooStretchedExp(const char *name, const char *title,
		  RooAbsReal& x, RooAbsReal& index, RooAbsReal& lambda);
  RooStretchedExp(const RooStretchedExp& other, const char* name=0);
  inline virtual TObject* clone(const char* newname) const
                               { return new RooStretchedExp(*this,newname); } //!< cloner
  inline virtual ~RooStretchedExp() { }   //!< Destructor

  Int_t    getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char*) const;
  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;
  Int_t    getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const;
  void     generateEvent(Int_t code);

protected:
  Double_t evaluate() const ;

protected:
  RooRealProxy _index;  
  RooRealProxy _lambda;  
  
private:
  ClassDef(RooStretchedExp,1); 
};

#endif  // end ROO_STRETCHEDEXP_HH

//  end RooStretchedExp.hh
//  ############################################################################################
