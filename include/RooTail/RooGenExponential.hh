//  ############################################################################################
//! @file       RooGenExponential.hh
//! @brief      Header for generalized exponential PDF
//! @author     "EmmaH" <dev@daemma.io>
//! @date       2018-05-28
//! @copyright  Copyright (C) 2017 "EmmaH" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
#ifndef ROO_GENEXPONENTIAL_HH
#define ROO_GENEXPONENTIAL_HH
#include "RooAbsTailPdf.hh"  // rootail: Abstract base PDF

//  ********************************************************************************************
//! Generalized exponential PDF.
/*! @f[ f(x) = \left( e^{\frac{|x-x_{c}|}{x_{s}}} + \eta \right)^{-1} @f] 
    For @f$ \eta = 0,1,-1 @f$ use RooGenExponential::DecayType = Exp, Fermi, Bose.
    \note Tail-science minded Re-implementation of "RooExponential" for consistancy.
*/
class RooGenExponential : public RooAbsTailPdf {
public:
  inline RooGenExponential(){};  //!< Default constructor
  RooGenExponential(const char *name, const char *title,
		    RooAbsReal& x, RooAbsReal& xScale, RooAbsReal& xCut);
  RooGenExponential(const RooGenExponential& other, const char* name=0);
  inline virtual TObject* clone(const char* newname) const
                               { return new RooGenExponential(*this,newname); } //!< cloner
  inline virtual ~RooGenExponential() { }   //!< Destructor

  Int_t    getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char*) const;
  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;
  Int_t    getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t) const;
  void     generateEvent(Int_t code);

  enum DecayType { Exp=0, Fermi=1, Bose=-1 };
  Int_t eta() const { return _Eta; }
  void  setEta(Int_t eta = 0);
  void  setEtaType(DecayType dt = Exp);

protected:
  Double_t evaluate() const ;

protected:
  RooRealProxy _xS;  
  RooRealProxy _xC;  
  Int_t        _Eta;
  
private:
  ClassDef(RooGenExponential,1); 
};

#endif  // end ROO_GENEXPONENTIAL_HH

//  end RooGenExponential.hh
//  ############################################################################################
